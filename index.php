﻿<?php
/**
 * @param Protty
 * @url www.pder.org
 **/
if (! file_exists("PROTTY.KEY")) {
    header("location:Install.php");
    exit();
}
session_start();
@include 'config.php';
@include 'index/list.index.php';
@include 'index/page.index.php';
@include 'index/site.index.php';
@include 'index/bower.index.php';
@include 'admin/operation/site.function.php';
$ver = sites($config);
$a_page = $ver['page_ment'];
$pageCount = PageCount($config, $a_page);
$n = 1;
if (! empty($_GET['page'])) {
    $n = $_GET['page'];
    if ($_GET['page'] < 1) {
        $n = 1;
    }
    if ($_GET['page'] > $pageCount) {
        $n = $pageCount;
    }
}
$page = Page($n, $a_page, $config);
$v = index_List($page, $a_page, $config);
$count = counts($config);
$site = siteid($config);
$link = siteLink($config);
$user = Img($config);
$url = "http://" . $_SERVER['HTTP_HOST'];
$c_url = $_SERVER['HTTP_HOST'];
?>
<html>
<head>

<title><?php echo $site['title']?></title>
<meta name="keywords" content="<?php echo $site['key_s']?>" />
<meta name="description" content="<?php echo $site['con']?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale= 0"/>
<link href="css/protty.css" rel="stylesheet">
<link rel="stylesheet" href="css/go.css">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
<link type="text/css" rel="stylesheet" href="css/link.css">
<link type="text/css" rel="stylesheet" href="css/lin.css">
<link type="text/css" rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="wrap">
		<header id="header" class="clearfix">
			<div class="grid header-menu">
				<div class="col-9-12">
					<nav id="nav-menu" class="clearfix" role="navigation">
						<a id="logo" href="" alt="轻击键盘，静候回音。">日记</a> <a href="<?php echo $url?>">首页</a>
						<?php if ($_SESSION['user_login']){?><a
							href="admin/index.php?protty=release" title="关于">后台管理</a><?php }else {?>
						<a href="admin" title="关于">登录</a>
						<?php }?>
					</nav>
				</div>
				<!-- <div class="col-3-12">
					<small>关注PROTTY，关注生活</small>
				</div> -->
			</div>
		</header>
		<!-- end #header -->
		<div id="content">
			<div class="grid">



				<div class="col-1-1 content-acticle">

					<div id="comments">
						<div class="ds-thread" id="ds-thread">
							<div id="ds-reset">
								<div class="ds-comments-info">
									<div class="ds-sort">
										<a class="ds-order-desc ds-current">记录你的点滴</a>
										<!-- <a
											class="ds-order-asc">最早</a><a class="ds-order-hot">最热</a> -->
									</div>
									<ul class="ds-comments-tabs">
										<li class="ds-tab">总共<span class="ds-highlight"><?php echo $count[count];?></span>条日记</a></li>
									</ul>
								</div>

								<ul class="ds-comments">


									<!-- POST -->
								<?php foreach ($v as $go){?>
									<?php if ($go['sys']==1){?><li class="ds-post"><div
											class="ds-post-self">
											<div class="ds-avatar">
												<img src="<?php echo $user['img'];?>" alt="AnyDrew">
											</div>

											<div class="ds-comment-body">
												<div class="ds-comment-header">
													<span class="ds-user-name"><?php echo $go['author'];?></span><span
														class="ds-user-level e-undefined"> <!-- <i
														class="icon icon-level1">英勇青铜</i> -->
													</span><span class="ds-user-agent"><span><i
															class="icon <?php echo $ico;?>"><?php echo $stm;?></i></span><span><i
															class="icon <?php echo $css;?>"><?php echo $bw;?></i></span></span>
												</div>
												<p><?php echo $go['content'];?></p>
												<div class="ds-comment-footer ds-comment-actions">
													<span class="ds-time" datetime="2013-12-07T15:30:43+08:00"
														title="2013年12月7日 下午3:30:43"><?php echo date("Y年m月d日",$go['times'])?></span>
													<!-- <a
														class="ds-post-reply" href="javascript:void(0);"><span
														class="ds-icon ds-icon-reply"></span>回复</a> -->
													<a target="_blank" class="ds-post-repost"
														href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=<?php echo $url;?>&title=<?php echo $go['content'];?>&desc=<?php echo $go['content'];?>"><span
														class="ds-icon ds-icon-share"></span>转发到QQ空间</a>
												</div>
											</div>

										</div></li><?php }?>
										<?php }?>
										<!-- end POST -->



								</ul>

								<div class="ds-paginator" style="display: block;">
									<div class="ds-border"></div>
									<a onclick='page();' data-page='1'
        href='?page=1' name='pages' class='ds-current'><</a>
									<?php 
								
									pagebar($pageCount, $n, 5);
									
									?>
									<a onclick='page();' data-page='1'
        href='?page=<?php echo $pageCount;?>' name='pages' class='ds-current'>></a>
								</div>
								<a name="respond"></a>


							</div>

						</div>

					</div>
				</div>
			</div>




			<div class="link">
			<?php foreach ($link as $ls){?>
	<a href="<?php echo $ls['link_url']?>?<?php echo $c_url;?>"
					title="<?php echo $ls['link_con']?>" target="_blank"><?php echo $ls['link_name']?></a>
	<?php }?>
</div>

		</div>
		<!-- end #body -->

		<footer id="footer">
			<div class="grid">
				<div class="col-9-12">
					<center>
						<small>&copy; 日记本</small>
					</center>
				</div>
				<!-- 				<div class="col-3-12">
					<a id="gotop" href="javascript:void(0);" onclick="return false;">TOP</a> 
									</div> -->
			</div>
		</footer>
		<!-- end #footer -->
	</div>

</body>
</html>